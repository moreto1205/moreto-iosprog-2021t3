using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPlayer : MonoBehaviour
{
    public GameObject[] charPrefab;
    public Transform spawnPoint;

    private void Start()
    {
        int SelectedChar = PlayerPrefs.GetInt("selected");
        GameObject prefab = charPrefab[SelectedChar];
        GameObject clone = Instantiate(prefab, spawnPoint.position, Quaternion.identity);
    }
}
