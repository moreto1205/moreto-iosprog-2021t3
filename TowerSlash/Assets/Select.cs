using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class Select : MonoBehaviour
{
    public GameObject[] characters;
    public int selected;

    private void Start()
    {
        selected = 0;
    }

    public void NextChar()
    {
        characters[selected].SetActive(false);
        selected = (selected + 1) % characters.Length;
        characters[selected].SetActive(true);
    }

    public void PrevChar()
    {
        characters[selected].SetActive(false);
        selected--;
        if (selected < 0)
        {
            selected += characters.Length;
        }

        characters[selected].SetActive(true);
    }

    public void StartGame()
    {
        PlayerPrefs.SetInt("selected", selected);
        SceneManager.LoadScene("Main", LoadSceneMode.Single);
    }
}
