using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public int randomDirection;
    PlayerMove swipeDirection;
    public GameObject[] arrow;
    public GameObject currentArrow;

    public bool playerInRange;

    // Start is called before the first frame update
    void Start()
    {
        randomDirection = Random.Range(1,4);
        Debug.Log(randomDirection);
        SpawnArrow();
    }

    // Update is called once per frame
    void Update()
    {
        if (playerInRange == true)
        {
            if (randomDirection == FindObjectOfType<PlayerMove>().Direction)
            {
                Destroy(gameObject);
                swipeDirection.score += 50;
                ScoreUi.Amount += 50;
                swipeDirection.Direction = 0;
                
            }

            else if(randomDirection != FindObjectOfType<PlayerMove>().Direction)
            {
                swipeDirection.hp -= 1;
                HpUi.Amount -= 1;
                swipeDirection.Direction = 0;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerInRange = true;
        }
    }

    void SpawnArrow()
    {
       Transform arrowSpawnPoint = transform.GetChild(1).transform;
       transform.GetChild(randomDirection).gameObject.SetActive(true);
    }

   
}
