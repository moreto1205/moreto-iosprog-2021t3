using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotateEnemy : MonoBehaviour
{
    public int randomDirection;
    PlayerMove swipeDirection;

    public bool playerInRange;

    // Start is called before the first frame update
    void Start()
    {
        randomDirection = Random.Range(1, 4);
        Debug.Log(randomDirection);
        StartCoroutine(RandomArrow());
    }

    // Update is called once per frame
    void Update()
    {
        if (playerInRange == true)
        {
            if (randomDirection == FindObjectOfType<PlayerMove>().Direction)
            {
                Destroy(gameObject);
                swipeDirection.score += 100;
                ScoreUi.Amount += 100;
            }
            else if(randomDirection != FindObjectOfType<PlayerMove>().Direction)
            {
                swipeDirection.hp -= 1;
                HpUi.Amount -= 1;
                swipeDirection.Direction = 0;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerInRange = true;
        }
    }

    public IEnumerator RandomArrow()
    {
        Transform arrowSpawnPoint = transform.GetChild(1).transform;
        while (true)
        {
            randomDirection = Random.Range(1, 4);
            transform.GetChild(randomDirection).gameObject.SetActive(true);
            yield return new WaitForSeconds(.1f);

            if (playerInRange == true)
            {
                break;
            }

            transform.GetChild(randomDirection).gameObject.SetActive(false);
        }

        yield return new WaitForEndOfFrame();
    }

}
