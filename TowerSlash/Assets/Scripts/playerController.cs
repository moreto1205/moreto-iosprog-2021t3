using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour
{
    private Vector3 fp;   //First touch position
    private Vector3 lp;   //Last touch position
    private float dragDistance;  //minimum distance for a swipe to be registered

    private int Direction;
    // 1 = right , 2 = left, 3 = up, 4 =down

    public float dashTime;
    public float startDashTime;
    public int dashSpeed;

    public Rigidbody2D rb;


    void Start()
    {
        dashTime = startDashTime;
        dragDistance = Screen.height * 15 / 100; //dragDistance is 15% height of the screen
    }

    void Update()
    {
        if (Input.touchCount == 1) // user is touching the screen with a single touch
        {
            Touch touch = Input.GetTouch(0); // get the touch
            if (touch.phase == TouchPhase.Began) //check for the first touch
            {
                fp = touch.position;
                lp = touch.position;
            }
            else if (touch.phase == TouchPhase.Moved) // update the last position based on where they moved
            {
                lp = touch.position;
            }
            else if (touch.phase == TouchPhase.Ended) //check if the finger is removed from the screen
            {
                lp = touch.position;  
                if (Mathf.Abs(lp.x - fp.x) > dragDistance || Mathf.Abs(lp.y - fp.y) > dragDistance)
                {
                    if (Mathf.Abs(lp.x - fp.x) > Mathf.Abs(lp.y - fp.y))
                    {   //If the horizontal movement is greater than the vertical movement
                        if ((lp.x > fp.x))  
                        {   //Right swipe
                            Debug.Log("Right Swipe");
                            InputUi.InputCommand = "Right Swipe";
                            Direction = 1;
                        }
                        else
                        {   //Left swipe
                            Debug.Log("Left Swipe");
                            InputUi.InputCommand = "Left Swipe";
                            Direction = 2;
                        }
                    }
                    else
                    {   //the vertical movement is greater than the horizontal movement
                        if (lp.y > fp.y)  //If the movement was up
                        {   //Up swipe
                            Debug.Log("Up Swipe");
                            InputUi.InputCommand = "Up Swipe";
                            Direction = 3;
                        }
                        else
                        {   //Down swipe
                            Debug.Log("Down Swipe");
                            InputUi.InputCommand = "Down Swipe";
                            Direction = 4;
                        }
                    }
                }
                else
                {   //It's a tap as the drag distance is less than 20% of the screen height
                    Debug.Log("Tap");
                    InputUi.InputCommand = "Tap";

                    if(dashTime <= 0 )
                    {
                        dashTime = startDashTime;
                        rb.velocity = Vector2.zero;
                    }
                    else
                    {
                        dashTime -= Time.deltaTime;
                        rb.velocity = Vector2.up * dashSpeed;
                        Debug.Log("Dash");
                    }

                }
            }
        }
    }

}
