using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextTileScript : MonoBehaviour
{
    [SerializeField]
    public GameObject[] tiles;

    private int prefabNum;
    public GameObject[] Players;
    public GameObject playerSpawn;

    Select charNum;

    Vector2 tileSpawnPoint;

    public void SpawnTile()
    {
        prefabNum = Random.Range(0, tiles.Length);
        GameObject temp = Instantiate(tiles[prefabNum], tileSpawnPoint, Quaternion.identity);
        tileSpawnPoint = temp.transform.GetChild(1).transform.position;
    }


    private void Start()
    {
        //Transform pSpawn = playerSpawn.transform;
        //Instantiate(Players[charNum.selected],pSpawn.position, Quaternion.identity);
        
       for (int i = 0; i < 6; i++)
        {
            SpawnTile();
        }

    }

    // Update is called once per frame
    void Update()
    {

    }
}
