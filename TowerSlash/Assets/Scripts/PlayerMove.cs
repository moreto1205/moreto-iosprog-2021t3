using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMove : MonoBehaviour
{
    public float speed;
    public Rigidbody2D rb;
    public int hp;
    public int score;

    private Vector3 fp;   //First touch position
    private Vector3 lp;   //Last touch position
    private float dragDistance;  //minimum distance for a swipe to be registered

    public int Direction;
    // 1 = right , 2 = left, 3 = up, 4 =down

    public float dashTime;
    public float startDashTime;
    public int dashSpeed;

    public bool isInvincible;
    // Start is called before the first frame update
    void Start()
    {
        isInvincible = false;
        Direction = -1;
        HpUi.Amount += hp;
        ScoreUi.Amount += score;
        dashTime = startDashTime;
        dragDistance = Screen.height * 15 / 100; //dragDistance is 15% height of the screen
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 1) // user is touching the screen with a single touch
        {
            Touch touch = Input.GetTouch(0); // get the touch
            if (touch.phase == TouchPhase.Began) //check for the first touch
            {
                fp = touch.position;
                lp = touch.position;
            }
            else if (touch.phase == TouchPhase.Moved) // update the last position based on where they moved
            {
                lp = touch.position;
            }
            else if (touch.phase == TouchPhase.Ended) //check if the finger is removed from the screen
            {
                lp = touch.position;
                if (Mathf.Abs(lp.x - fp.x) > dragDistance || Mathf.Abs(lp.y - fp.y) > dragDistance)
                {
                    if (Mathf.Abs(lp.x - fp.x) > Mathf.Abs(lp.y - fp.y))
                    {   //horizontal movement is greater than the vertical movement
                        if ((lp.x > fp.x))
                        {   //Right swipe
                            Debug.Log("Right Swipe");
                            InputUi.InputCommand = "Right Swipe";
                            Direction = 1;
                        }
                        else
                        {   //Left swipe
                            Debug.Log("Left Swipe");
                            InputUi.InputCommand = "Left Swipe";
                            Direction = 2;
                        }
                    }
                    else
                    {   //vertical movement is greater than the horizontal movement
                        if (lp.y > fp.y)  //movement was up
                        {   //Up swipe
                            Debug.Log("Up Swipe");
                            InputUi.InputCommand = "Up Swipe";
                            Direction = 3;
                        }
                        else
                        {   //Down swipe
                            Debug.Log("Down Swipe");
                            InputUi.InputCommand = "Down Swipe";
                            Direction = 4;
                        }
                    }
                }
                else
                {   //It's a tap as the drag distance is less than 20% of the screen height
                    Debug.Log("Tap");
                    InputUi.InputCommand = "Tap";

                    if (dashTime <= 0)
                    {
                        dashTime = startDashTime;
                        rb.velocity = Vector2.zero;
                    }
                    else
                    {
                        dashTime -= Time.time;
                        rb.velocity = Vector2.up * dashSpeed;
                        Debug.Log("Dash");
                    }

                }
            }
        }
        
        transform.Translate(Vector2.up * speed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Hp")

        {
            Destroy(collision.gameObject);
            hp += 1;
            HpUi.Amount += 1;

        }

        if (collision.gameObject.tag == "Power-up")

        {
            Destroy(collision.gameObject);
            score += 100;
            ScoreUi.Amount += 100;

            if (dashTime <= 0)
            {
                isInvincible = false;
                dashTime = startDashTime;
                rb.velocity = Vector2.zero;
            }
            else
            {
                isInvincible = true;
                dashTime -= Time.time;
                rb.velocity = Vector2.up * dashSpeed;
                Debug.Log("Dash");
            }

        }

        if (collision.gameObject.tag == "Enemy")

        {
            Destroy(collision.gameObject);
            if (isInvincible == false)
            {
                hp -= 1;
                HpUi.Amount -= 1;

                if (hp <= 0)
                {
                    Death();
                }
            }
        }
    }
    void Death()
    {
        Destroy(gameObject);
        SceneManager.LoadScene("Death", LoadSceneMode.Single);
    }

  
}
