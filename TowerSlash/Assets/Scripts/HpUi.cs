using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HpUi : MonoBehaviour
{
    public Text Hptxt;
    public static int Amount = 0;
    // Start is called before the first frame update
    void Start()
    {
        Hptxt = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        Hptxt.text = "Hp: " + Amount.ToString();
    }
}
