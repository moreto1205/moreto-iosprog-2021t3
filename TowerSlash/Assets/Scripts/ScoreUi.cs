using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreUi : MonoBehaviour
{
    public Text Scoretxt;
    public static int Amount = 0;
    // Start is called before the first frame update
    void Start()
    {
        Scoretxt = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        Scoretxt.text = "Score:" + Amount.ToString();
    }
}
