using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileGenerate : MonoBehaviour
{
    NextTileScript spawner;
    public GameObject[] enemy;

    private void Start()
    {
        spawner = FindObjectOfType<NextTileScript>();
        SpawnEnemy();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //Debug.Log("exited");
            spawner.SpawnTile();
            Destroy(gameObject, 5);
        }
    }

    void SpawnEnemy()
    {
        int x = Random.Range(0, enemy.Length);
        Transform EnemySpawn = transform.GetChild(9).transform;
        Instantiate(enemy[x], EnemySpawn.position, enemy[x].transform.rotation, transform);
    }
}
