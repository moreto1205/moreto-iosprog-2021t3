﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum MenuType
{
    Start,
    Main,
    Over,
}
public class MenuCanvas : MonoBehaviour
{

    [SerializeField] MenuType menuType;

    public MenuType MenuType { get { return menuType;  } }

    // Start is called before the first frame update
    protected virtual void Start()
    {
        MenuManager.Instance.RegisterMenu(this);
    }

    public void show()
    {
        if(this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(true);
        }
    }   
    
    public void hide()
    {
        if(this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(false);
        }
    }

}
