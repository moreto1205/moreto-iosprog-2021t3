﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using System;

public class AiManager : MonoBehaviour
{
    private AIDestinationSetter playerChaseBehavior;
    private Patrol patrolBehavior;
    public enemyStats shootingBehavior;
    public float firerate;
    public int fireCount;

    // Start is called before the first frame update
    void Start()
    {
        playerChaseBehavior = GetComponent<AIDestinationSetter>();
        patrolBehavior = GetComponent<Patrol>();
        shootingBehavior.primary.curAmmo += 100;
        AmmoCount.amount = 0;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Enemy") || collision.gameObject.tag.Equals("Player"))
        {
            playerChaseBehavior.enabled = true;
            patrolBehavior.enabled = false;
            InvokeRepeating("AIshoot", 0.5f, firerate);
            AiReload();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Enemy") || collision.gameObject.tag.Equals("Player"))
        {
            playerChaseBehavior.enabled = false;
            patrolBehavior.enabled = true;
            CancelInvoke();
        }
    }

    public void AIshoot()
    {
        shootingBehavior.shoot();
        AmmoCount.amount += 1;
        Debug.Log("firing");
        fireCount += 1;
    }

    public void AiReload()
    {
        if(shootingBehavior.primary is Shotgun)
        {
            if( fireCount == 2)
            {
                CancelInvoke();
                fireCount = 0;
                shootingBehavior.primary.curAmmo += 2;
            }
            InvokeRepeating("AIshoot", 10f, firerate);
        }

       else  if (shootingBehavior.primary is Pistol)
        {
            if (fireCount == 10)
            {
                CancelInvoke();
                fireCount = 0;
                shootingBehavior.primary.curAmmo += 10;
            }
            InvokeRepeating("AIshoot", 3f, firerate);
        }

        else if (shootingBehavior.primary is Rifle)
        {
            if (fireCount == 30)
            {
                CancelInvoke();
                fireCount = 0;
                shootingBehavior.primary.curAmmo += 30;
            }
            InvokeRepeating("AIshoot", 5f, firerate);
        }

        Debug.Log("reloaded");
    }

}
