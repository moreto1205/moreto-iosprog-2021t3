﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : Singleton<MenuManager>
{
    [SerializeField] List<MenuCanvas> canvasList = new List<MenuCanvas>();

    public void RegisterMenu(MenuCanvas menuCanvas)
    {
        canvasList.Add(menuCanvas);
        menuCanvas.hide();
    }

    public void hideAll()
    {
        foreach(MenuCanvas menuCanvas in canvasList)
        {
            menuCanvas.hide();
        }
    }

    public void showCanvas( MenuType menuType)
    {
        hideAll();

        foreach (MenuCanvas menuCanvas in canvasList)
        {
            if(menuCanvas.MenuType == menuType)
            {
                menuCanvas.show();
                break;
            }
        }
    }
}
