﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class enemyStats : Unit
{
    public float rate;
    public PlayerStats pKills;
    int damageTake;

    // Start is called before the first frame update
    void Start()
    {
        Init(100, 10f);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "bullet")
        {
            DamageTaken(10);

            if (hp.CurHp <= 0)
            {
                Destroy(gameObject);
                pKills.Kills += 1;

            }
        }

        else if(collision.gameObject.tag == "Rbullet")
        {
            DamageTaken(15);

            if (hp.CurHp <= 0)
            {
                Destroy(gameObject);
                pKills.Kills += 1;
            }
        }

        else if (collision.gameObject.tag == "SBullet")
        {
            DamageTaken(8);

            if (hp.CurHp <= 0)
            {
                Destroy(gameObject);
                pKills.Kills += 1;
            }
        }
    }



        
}
