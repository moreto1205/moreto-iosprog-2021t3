﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class GameManager : Singleton<GameManager>
{
    // Start is called before the first frame update
    void Start()
    {
        loadScene("Start");
        loadScene("Main");
        loadScene("Restart");
    }

    void loadScene(string name)
    {
        StartCoroutine(AsyncLoadScene(name, onMenuLoaded));
    }

    void onMenuLoaded()
    {
        MenuManager.Instance.showCanvas(MenuType.Start);
    }

    IEnumerator AsyncLoadScene(string name, Action onCallBack  = null)
    {
        AsyncOperation asyncLoadScene = SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);

        while (!asyncLoadScene.isDone)
        {
            yield return null;
        }
    }
}
