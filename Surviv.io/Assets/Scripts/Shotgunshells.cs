﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgunshells : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            ShotAmmoCount.amount += 4;
            collision.GetComponent<PlayerStats>().shotgunAmmo += 4;
            Debug.Log("Ammo added");
            Destroy(gameObject);
        }

        if (collision.gameObject.tag == "Enemy")
        {
            Destroy(gameObject);
        }
    }
}
