﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawn : MonoBehaviour
{
    public GameObject[] items;
    public int itemAmount;
    private void Awake()
    {
        spawnItems();
    }

    public void spawnItems()
    {
        bool itemspawned = false;
        while (!itemspawned)
        {
            for (int j = 0; j < itemAmount; j++)
            {
                for (int i = 0; i < items.Length; i++)
                {
                    Vector3 itemPos = new Vector3(Random.Range(-40f, 40f), Random.Range(-35f, 35f), 0f);
                    if ((itemPos - this.transform.position).magnitude < 8)
                    {
                        continue;
                    }
                    else
                    {
                        GameObject Prefabs = Instantiate(items[i], itemPos, Quaternion.identity) as GameObject;
                        itemspawned = true;
                    }
                }
            }
        }
    }
}
