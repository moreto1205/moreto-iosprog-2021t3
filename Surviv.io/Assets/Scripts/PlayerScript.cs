﻿using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    float Xmove = 0f;
    float Ymove = 0f;
    private Rigidbody2D rb;
    Vector2 movement;
    public float speed = 2f;

    public Joystick jt;
    public Transform firepoint;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Xmove = SimpleInput.GetAxis("Horizontal");
        Ymove = SimpleInput.GetAxis("Vertical");

        rb.velocity = new Vector2(Xmove * speed, Ymove * speed);

        transform.position = new Vector2(Mathf.Clamp(transform.position.x, -27.92825f, 29.03381f),
            Mathf.Clamp(transform.position.y, -22.16242f, 22.04932f));

        Vector3 moveVector = (Vector3.up * jt.Vertical - Vector3.left * jt.Horizontal);
        if (jt.Horizontal != 0 || jt.Vertical != 0)
        {
            transform.rotation = Quaternion.LookRotation(Vector3.forward, moveVector);
        }

    }

   

    // create Ammo class under Inventory script

}

