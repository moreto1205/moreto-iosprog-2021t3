﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class PlayerStats : Unit
{
    private Rigidbody2D rb;

    public Transform dropPoint;

    public int weaponCount;

    public int damageApplied;

    public Action PlayerDeath;

    public bool isfiring = false;

    public bool canReload = false;

    public int Kills = 0;

    [SerializeField] private Image Ppistol;
    [SerializeField] private Image Spistol;
    [SerializeField] private Image PShot;
    [SerializeField] private Image SShot;
    [SerializeField] private Image PAr;
    [SerializeField] private Image SAr;

    // Start is called before the first frame update

    void Start()
    {
        Init(100f, 10f);
        rb = GetComponent<Rigidbody2D>();
        pistolAmmo = 0;
        rifleAmmo = 0;
        shotgunAmmo = 0;
        primary.curAmmo = 0;
        secondary.curAmmo = 0;
        AmmoCount.amount = primary.curAmmo;
        RifleAmmoCount.amount = rifleAmmo;
        PistolAmmoCount.amount = pistolAmmo;
        ShotAmmoCount.amount = shotgunAmmo;
    }

    // Update is called once per frame
    void Update()
    {
        weaponUiCheck();

        if (Kills > 9)
        {
            playerWin();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "bullet")
        {
            DamageTaken(10);

            if (hp.CurHp <= 0)
            {
                Destroy(gameObject);
                playerDeath();
            }
        }

        else if (collision.gameObject.tag == "Rbullet")
        {
            DamageTaken(15);

            if (hp.CurHp <= 0)
            {
                Destroy(gameObject);
                playerDeath();
            }
        }

        else if (collision.gameObject.tag == "SBullet")
        {
            DamageTaken(8);

            if (hp.CurHp <= 0)
            {
                Destroy(gameObject);
                playerDeath();
            }
        }
    }

    void playerDeath()
    {
        SceneManager.LoadScene("Restart");
        //load restart menu
    }

    void playerWin()
    {
        SceneManager.LoadScene("Win");
    }
  
    public void drop()
    {
        temp = secondary;
        primary.drop();
        primary = temp;
        secondary = null;
        weaponCount --;
    }

    public void swapGun()
    {
        temp = primary;
        primary = secondary;
        secondary = temp;

        temp.curAmmo = primary.curAmmo;
        primary.curAmmo = secondary.curAmmo;
        secondary.curAmmo = temp.curAmmo;

        AmmoCount.amount = primary.curAmmo;
    }

  
    public void suicide()
    {
        DamageTaken(20);
        if(hp.CurHp <=0)
        {
            Destroy(this);
            playerDeath();
        }
    }

    public void weaponUiCheck()
    {
        if (primary is Pistol)
        {
            Ppistol.enabled = true;
            PShot.enabled = false;
            PAr.enabled = false;
            Spistol.enabled = false;
        }

        if (primary is Shotgun)
        {
            Ppistol.enabled = false;
            PShot.enabled = true;
            PAr.enabled = false;
            SShot.enabled = false;
        }

        if (primary is Rifle)
        {
            Ppistol.enabled = false;
            PAr.enabled = false;
            PShot.enabled = false;
            PAr.enabled = true;
            SAr.enabled = false;
        }

        if (secondary is Pistol)
        {
            Spistol.enabled = true;
            SShot.enabled = false;
            SAr.enabled = false;
        }

        if (secondary is Shotgun)
        {
            SShot.enabled = false;
            SShot.enabled = true;
            SAr.enabled = false;
        }

        if (secondary is Rifle)
        {
            SAr.enabled = false;
            SShot.enabled = false;
            SAr.enabled = true;
        }

        if (primary is null)
        {
            Ppistol.enabled = false;
            PShot.enabled = false;
            PAr.enabled = false;
        }

        if(secondary is null)
        {
            Spistol.enabled = false;
            SShot.enabled = false;
            SAr.enabled = false;
        }

    }


}
