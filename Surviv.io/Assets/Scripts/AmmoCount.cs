﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmmoCount : MonoBehaviour
{
    public Text txt;
    public static int amount = 0;

    // Start is called before the first frame update
    void Start()
    {
        txt = GetComponent<Text>();
        amount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        txt.text = "Ammo: " + amount.ToString();
    }
}
