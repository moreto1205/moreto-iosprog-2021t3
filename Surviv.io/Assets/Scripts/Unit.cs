﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HP))]
public class Unit : MonoBehaviour
{
    public HP hp;
    public float speed;

    public Weapons primary;
    public Weapons secondary;
    public Weapons temp;

    public Transform firepoint;

    public int rifleAmmo;
    public int shotgunAmmo;
    public int pistolAmmo;


    public virtual void Init(float mHp, float mSpeed)
    {
        hp = this.GetComponent<HP>();
        this.speed = mSpeed;

        hp.Init(mHp);
    }

    public void DamageTaken(float dmg)
    {
        hp.TakeDamage(dmg);
    }

    public void shoot()
    {
        if (primary.curAmmo > 0)
        {
            primary.Fire(firepoint);
            Debug.Log("firing");
        }
        // calls fire from weapon class
    }

    public void reload()
    {
        if (primary is Pistol)
        {
           primary.Wreload(pistolAmmo);
        }

        else if (primary is Shotgun)
        {
           primary.Wreload(shotgunAmmo);
        }

        else if (primary is Rifle)
        {
            primary.Wreload(rifleAmmo);
        }
          

    }

    public void stopFire()
    {
        primary.NotShootingGun(firepoint);
    }

    void Start()
    {

    }

    void Update()
    {
        unitDeath();
    }

    void unitDeath()
    {
        if(hp.CurHp < 0)
        {
            Destroy(this);
        }
    }

    

    
}
