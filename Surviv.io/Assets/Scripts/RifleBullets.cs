﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RifleBullets : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            RifleAmmoCount.amount += 30;
            collision.GetComponent<PlayerStats>().rifleAmmo += 30;
            Debug.Log("Ammo added");
            Destroy(gameObject);
        }

        if (collision.gameObject.tag == "Enemy")
        {
            Destroy(gameObject);
        }
    }
}
