﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapons : MonoBehaviour
{
    //private WeaponType weaponType;
    public int damage;
    public int weaponId;
    // 0 pistol
    // 1 shottie
    // 2 ar

    public int MaxAmmo;
    public int reloadtime = 1;
    public int curAmmo;

    public bool isReloading;
   
    void Start()
    {
        if(curAmmo == -1)
        {
            curAmmo = MaxAmmo;
        }
    }
    public void Init(int mammosize, int mdamage)
    {
        this.MaxAmmo = mammosize;
        this.damage = mdamage;
    }

    public virtual void Fire(Transform mFire) //int curAmmo
    {
        curAmmo--;
        AmmoCount.amount--;
       // GetComponent<PlayerStats>().canReload = false;
        // Override Instatiation of bullets in weapons
    }

    public virtual void Wreload(int clipToLoad)
    {
      //override reload on each weapon
    }

    public virtual void NotShootingGun(Transform mFire)
    {
      //rifle stop override
    }

    public void drop()
    {
        transform.position = transform.parent.GetComponent<PlayerStats>().dropPoint.position;
        transform.parent.GetComponent<PlayerStats>().primary = null;
        transform.parent = null;
        this.GetComponent<SpriteRenderer>().enabled = true;
    }
}


