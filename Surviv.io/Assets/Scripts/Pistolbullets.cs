﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistolbullets : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            PistolAmmoCount.amount += 10;
            collision.GetComponent<PlayerStats>().pistolAmmo += 10;
            Debug.Log("Ammo added");
            Destroy(gameObject);
        }

        if (collision.gameObject.tag == "Enemy")
        {
            Destroy(gameObject);
        }
    }

        
}
