﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShotAmmoCount : MonoBehaviour
{
    public Text Shottxt;
    public static int amount = 0;

    // Start is called before the first frame update
    void Start()
    {
        Shottxt = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        Shottxt.text = " " + amount.ToString();
    }
}
