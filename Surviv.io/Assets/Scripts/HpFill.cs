﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HpFill : MonoBehaviour
{
    public Slider slider;

    // Start is called before the first frame update
    void Start()
    {
        slider = GetComponent<Slider>();
    }

    // Update is called once per frame
    public void setMaxHp(float hp)
    {
        slider.maxValue = hp;
        slider.value = hp;
    }

    public void setHealth(float hp)
    {
        slider.value = hp;
    }
}
