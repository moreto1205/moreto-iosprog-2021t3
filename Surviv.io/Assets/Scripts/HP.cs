﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HP : MonoBehaviour
{
    public float CurHp { get; set; }
    public float maxHealth { get { return maxHp; } }

    public HpFill healthFill;

    float maxHp;

    // Start is called before the first frame update
    void Start()
    {
        CurHp = maxHp;
        healthFill.setMaxHp(maxHealth);
    }

    public void Init(float maxHealth)
    {
        this.maxHp = maxHealth;
        CurHp = maxHealth;
    }

    public void TakeDamage(float value)
    {
        CurHp -= value;

        healthFill.setHealth(CurHp);

        if (CurHp > maxHealth)
        {
            CurHp = maxHealth;
        }

    }
  
}
