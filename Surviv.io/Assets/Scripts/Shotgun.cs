﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : Weapons
{
    public Transform Firepoint;
    public GameObject bulletPrefab;
    public GameObject player;
    public float bulletSpeed;
    public float bulletforce;
    
    public int BulletsFired;
    public float ShotSpread;

    public PlayerStats playerStats;
    // Start is called before the first frame update
    void Start()
    {
        Init(2, 10);
       AmmoCount.amount =curAmmo;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void Fire(Transform mFire)
    {
        {
            base.Fire(mFire);

            {
                GameObject bullet = Instantiate(bulletPrefab, mFire.position, mFire.rotation);
                Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
                rb.AddForce(mFire.up * bulletSpeed, ForceMode2D.Impulse);
            }
            //float TotalSpread = ShotSpread / BulletsFired;
            //for (int i = 0; i < BulletsFired; i++)
            //{
            //    // Calculate angle of this bullet
            //    float spreadA = TotalSpread * (i + 1);
            //    float spreadB = ShotSpread/ 2.0f;
            //    float spread = spreadB - spreadA + TotalSpread / 2;
            //    float angle = transform.eulerAngles.y;

            //    // Create rotation of bullet
            //    Quaternion rotation = Quaternion.Euler(new Vector3(0, spread + angle, 0));

            //    // Create bullet
            //    GameObject bullet = Instantiate(bulletPrefab, mFire.position, mFire.rotation);
            //    Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            //    rb.AddForce(mFire.up * bulletforce, ForceMode2D.Impulse);
            //    Debug.Log("Cone Shot");
            //}
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (collision.GetComponent<PlayerStats>().weaponCount == 0)
            { 
                this.transform.parent = collision.transform;
                transform.localRotation = Quaternion.identity;
                transform.localPosition = Vector3.zero;
                //transform.localScale = Vector3.one;
                this.GetComponent<SpriteRenderer>().enabled = false;
                collision.GetComponent<PlayerStats>().primary = this;
                collision.GetComponent<PlayerStats>().weaponCount = 1;
            }

            else if (collision.GetComponent<PlayerStats>().weaponCount == 1)
            {
                this.transform.parent = collision.transform;
                transform.localRotation = Quaternion.identity;
                transform.localPosition = Vector3.zero;
                //transform.localScale = Vector3.one;
                this.GetComponent<SpriteRenderer>().enabled = false;
                collision.GetComponent<PlayerStats>().secondary = this;
                collision.GetComponent<PlayerStats>().weaponCount = 2;
            }
        }
    }

    public override void Wreload(int clipToLoad)
    {
        if (playerStats.shotgunAmmo > 0)
        {
            if (playerStats.shotgunAmmo < MaxAmmo)
            {
                for (int i = 0; i < playerStats.shotgunAmmo; i++)
                {
                    playerStats.primary.curAmmo++;
                    AmmoCount.amount++;
                    playerStats.shotgunAmmo--;
                    ShotAmmoCount.amount--;
                }
            }

            else
            {
                for (int i = 0; i < MaxAmmo; i++)
                {
                    playerStats.primary.curAmmo++;
                    AmmoCount.amount++;
                    playerStats.shotgunAmmo--;
                    ShotAmmoCount.amount--;
                }
            }
        }
        else
        {

        }
        base.Wreload(clipToLoad);
    }


}
