﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{

    public Weapons gun;
    public GameObject [] loadout;

    public int rifleAmmo;
    public int shotgunAmmo;
    public int pistolAmmo;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Rifle Bullets")
        {
            rifleAmmo += 30;
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.tag == "9mm Bullets")
        {
            pistolAmmo += 15;
            Destroy(collision.gameObject);
            
        }

        if (collision.gameObject.tag == "Shotgun Shells")
        {
            shotgunAmmo += 4;
            Destroy(collision.gameObject);
        }
    }
}
