﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocChange : MonoBehaviour
{
    public float rate;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("change", 0.5f, rate);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void change()
    {
        Vector2 newLoc = new Vector2(Random.Range(-50f, 50f), Random.Range(-40f, 40f));
        this.transform.position = newLoc;
    }
}
