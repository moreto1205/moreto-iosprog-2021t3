﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RifleAmmoCount : MonoBehaviour
{
    public Text rfatxt;
    public static int amount = 0;

    // Start is called before the first frame update
    void Start()
    {
        rfatxt = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        rfatxt.text = "" + amount.ToString();
    }
}
