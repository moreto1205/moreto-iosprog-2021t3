﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PistolAmmoCount : MonoBehaviour
{
    public Text Ptxt;
    public static int amount = 0;

    // Start is called before the first frame update
    void Start()
    {
        Ptxt = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        Ptxt.text = " " + amount.ToString();
    }
}
