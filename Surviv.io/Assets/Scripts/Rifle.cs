﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rifle : Weapons
{
    public GameObject bulletPrefab;

    public Transform firepoint;

    public float bulletSpeed;

    public PlayerStats playerStats;

    public bool isARFiring;

    public float firerate;

  
    // Start is called before the first frame update
    void Start()
    {
        Init(30, 10);
        AmmoCount.amount =curAmmo;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void Fire(Transform mFire)
    { 
      base.Fire(mFire);
      isARFiring = true;
      {
        StartCoroutine(RifleFire(mFire));
      } 
    }

    public IEnumerator RifleFire(Transform mFire)
    {
        //if (isARFiring == true)
        {
            while (isARFiring == true)
            {
                GameObject bullet = Instantiate(bulletPrefab, mFire.position, mFire.rotation);
                Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
                rb.AddForce(mFire.up * bulletSpeed, ForceMode2D.Impulse);
                yield return new WaitForSeconds(0.25f);
                playerStats.primary.curAmmo--;
                AmmoCount.amount--;
            }
        }
    }

    public override void NotShootingGun(Transform mFire)
    {
        isARFiring = false;
        //StopCoroutine(RifleFire(mFire));
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (collision.GetComponent<PlayerStats>().weaponCount == 0)
            {
                this.transform.parent = collision.transform;
                transform.localRotation = Quaternion.identity;
                transform.localPosition = Vector3.zero;
                this.GetComponent<SpriteRenderer>().enabled = false;
                collision.GetComponent<PlayerStats>().primary = this;
                collision.GetComponent<PlayerStats>().weaponCount = 1;
            }

            else if (collision.GetComponent<PlayerStats>().weaponCount == 1)
            {
                this.transform.parent = collision.transform;
                transform.localRotation = Quaternion.identity;
                transform.localPosition = Vector3.zero;
                this.GetComponent<SpriteRenderer>().enabled = false;
                collision.GetComponent<PlayerStats>().secondary = this;
                collision.GetComponent<PlayerStats>().weaponCount = 2;
            }
        }
    }

    public override void Wreload(int clipToLoad)
    {
        if (playerStats.rifleAmmo > 0)
        {
            if (playerStats.rifleAmmo < MaxAmmo)
            {
                for (int i = 0; i < playerStats.rifleAmmo; i++)
                {
                    playerStats.primary.curAmmo++;
                    AmmoCount.amount++;
                    playerStats.rifleAmmo--;
                    RifleAmmoCount.amount--;
                }
            }
            else
            {
                for (int i = 0; i < MaxAmmo; i++)
                {
                    playerStats.primary.curAmmo++;
                    AmmoCount.amount++;
                    playerStats.rifleAmmo--;
                    RifleAmmoCount.amount--;
                }
            }
        }
        else
        {

        }
        base.Wreload(clipToLoad);
    }
}
