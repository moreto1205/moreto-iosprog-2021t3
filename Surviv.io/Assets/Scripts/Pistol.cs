﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pistol : Weapons
{
    public GameObject bulletPrefab;

    public Transform firepoint;

    public float bulletSpeed;

    public int pDamage;

    public PlayerStats playerStats;
    public Unit units;
    // Start is called before the first frame update
    void Start()
    {
        Init(15, 10);
        AmmoCount.amount =curAmmo;
        //pDamage = GetComponent<Bullet>().damage = 10;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void Fire(Transform mFire)
    {
        
        {
            base.Fire(mFire);
            GameObject bullet = Instantiate(bulletPrefab, mFire.position, mFire.rotation);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(mFire.up * bulletSpeed, ForceMode2D.Impulse);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (collision.GetComponent<PlayerStats>().weaponCount == 0)
            {
                this.transform.parent = collision.transform;
                transform.localRotation = Quaternion.identity;
                transform.localPosition = Vector3.zero;
                this.GetComponent<SpriteRenderer>().enabled = false;
                collision.GetComponent<PlayerStats>().primary = this;
                collision.GetComponent<PlayerStats>().weaponCount = 1;
                //Ppistol.enabled = true;
            }

            else if (collision.GetComponent<PlayerStats>().weaponCount == 1)
            {
                this.transform.parent = collision.transform;
                transform.localRotation = Quaternion.identity;
                transform.localPosition = Vector3.zero;
                //transform.localScale = Vector3.one;
                this.GetComponent<SpriteRenderer>().enabled = false;
                collision.GetComponent<PlayerStats>().secondary = this;
                collision.GetComponent<PlayerStats>().weaponCount = 2;
                //Spistol.enabled = true;
            }
        }
        // check first on what you collide into
        // reparent and access through get component scipt and adding the weapon through this
    }

    public override void Wreload(int clipToLoad)
    {
        if(playerStats.pistolAmmo > 0)
        {
            if (playerStats.pistolAmmo < MaxAmmo)
            {
                for (int i = 0; i < playerStats.pistolAmmo; i++)
                {
                    playerStats.primary.curAmmo++;
                    AmmoCount.amount = playerStats.primary.curAmmo;
                    playerStats.pistolAmmo--;
                    PistolAmmoCount.amount = playerStats.pistolAmmo;
                }
            }

            else 
            {
                for (int i = 0; i < MaxAmmo; i++)
                {
                    playerStats.primary.curAmmo++;
                    AmmoCount.amount = playerStats.primary.curAmmo;
                    playerStats.pistolAmmo--;
                    PistolAmmoCount.amount = playerStats.pistolAmmo;
                }
            }
        }
        else
        {

        }
        base.Wreload(clipToLoad);
    }



}
