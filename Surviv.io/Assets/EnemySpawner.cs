﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject[] enemies;

    public int enemysize;
    private void Awake()
    {
        spawnAI();
    }

    public void spawnAI()
    {
        bool aiSpawn = false;
        while (!aiSpawn)
        {
            for (int j = 0; j < enemysize; j++)
            {
                for (int i = 0; i < enemies.Length; i++)
                {
                    Vector3 itemPos = new Vector3(Random.Range(-15f, 15f), Random.Range(-20f, 20f), 0f);
                    if ((itemPos - this.transform.position).magnitude < 8)
                    {
                        continue;
                    }
                    else
                    {
                        GameObject Prefabs = Instantiate(enemies[i], itemPos, Quaternion.identity) as GameObject;
                        aiSpawn = true;
                    }
                }
            }
        }
    }
}
